# Gulp with Livereload #

HTML/CSS for 1200px wide content, multi-device responsive framework.

Livereload with JS and CSS bundle to dist/

### How to use ###

You will need to install Gulp globally to run the command:

```
npm install -g gulp


```

Then install all dependencies from npm:


```
npm install


```


### Dependencies ###

    "gulp": "^3.9.1",
    "gulp-autoprefixer": "^3.1.1",
    "gulp-changed": "^1.3.0",
    "gulp-clean-css": "^2.0.6",
    "gulp-concat": "^2.6.0",
    "gulp-server-livereload": "^1.7.4",
    "gulp-sourcemaps": "^1.6.0",
    "gulp-uglify

### How to run ###

Build to dist/ and run on local server:

```
gulp

```

Build to dist/ without local server

```
gulp build

```

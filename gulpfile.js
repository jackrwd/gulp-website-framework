var gulp = require('gulp');
var prefix = require('gulp-autoprefixer');
var cleanCSS = require('gulp-clean-css');
var changed = require('gulp-changed');
var server = require('gulp-server-livereload');
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');
var uglify = require('gulp-uglify');

var source = [
    'src/.*',
    'src/favicon.ico',
    'src/robots.txt',
    'src/*.html',
    'src/**/*.html'
];

gulp.task('js', ['css'],function() {
    gulp.src(['src/js/jquery_1-11-1.js', 'src/js/*.js'])
        .pipe(sourcemaps.init())
        .pipe(concat('bundle.js'))
        .pipe(sourcemaps.write())
        .pipe(uglify())
        .pipe(changed('dist/js'))
        .pipe(gulp.dest('dist/js'));
});

gulp.task('dist', function() {
    gulp.src(source, {
            base: 'src'
        })
        .pipe(changed('dist'))
        .pipe(gulp.dest('dist'));
});

gulp.task('img', ['dist'], function() {
    gulp.src(['src/img/**'], {
            base: 'src/img'
        })
        .pipe(changed('dist/img'))
        .pipe(gulp.dest('dist/img'));
});

gulp.task('css', ['img'], function() {
    gulp.src(['src/css/basiclayout.css', 'src/css/*.css'])
        .pipe(sourcemaps.init())
        .pipe(concat('bundle.css'))
        .pipe(prefix('last 2 versions'))
        .pipe(cleanCSS())
        .pipe(changed('dist/css'))
        .pipe(gulp.dest('dist/css'));
});

gulp.task('serve', ['css', 'js'],function() {
    gulp.src('dist')
        .pipe(server({
            host: 'localhost',
            livereload: true,
            open: true
        }))
        .pipe(server({
            host: '0.0.0.0',
            livereload: true
        }));
});

gulp.task('watch', ['serve'],function() {
    gulp.watch(['src/js/**'], ['js']);
    gulp.watch(['src/css/**'], ['css']);
    gulp.watch(['src/img/**'], ['img']);
    gulp.watch(source, ['dist']);
});

gulp.task('build', ['dist', 'img', 'css', 'js']);

gulp.task('default', ['build', 'serve', 'watch']);
